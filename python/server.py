import time
from concurrent import futures
from datetime import datetime

import fortune_cookie
import grpc

import example_pb2
import example_pb2_grpc


class ExampleServicer(example_pb2_grpc.ExampleServiceServicer):
    def SayHello(self, request, context):
        response = example_pb2.HelloResponse()
        response.reply = "Hello " + request.greeting + "!"
        return response

    def DoAddition(self, request, context):
        response = example_pb2.NumberResponse()
        response.result = request.first_number + request.second_number
        return response

    def DoSubtraction(self, request, context):
        response = example_pb2.NumberResponse()
        response.result = request.first_number - request.second_number
        return response

    def CreateAsciiIdCard(self, request, context):
        print(request)
        start = time.time()
        response = example_pb2.AsciiIdCardResponse()
        request.epoch_birthdate = datetime.fromtimestamp(int(request.epoch_birthdate)).strftime('%d-%m-%Y')
        if len(request.name) > 20:
            request.name = f'{request.name[:17]}...'
        if len(request.occupation) > 20:
            request.occupation = f'{request.occupation[:17]}...'
        response.result = \
            ' _________________________________________________ \n' + \
            '|                                   ____________  |\n' + \
            f'| Name       :{request.name: <20} |   _____    | |\n' + \
            f'| Birthdate  :{request.epoch_birthdate: <20} | /  ^   ^\\  | |\n' + \
            f'| Gender     :{"Male" if request.gender == 0 else "Female": <20} ||e  O   O | | |\n' + \
            f'| Marriage   :{example_pb2.MarriageStatus.Name(request.marriage).replace("_", " ").capitalize(): <20}' + \
            ' | \\_   --_/  | |\n' + \
            f'| Occupation :{request.occupation: <20} |   |   |    | |\n' + \
            '|                                   ------------  |\n' + \
            ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '
        response.todays_fortune = fortune_cookie.fortune()
        time.sleep(0.1)
        response.execution_time = str(time.time() - start - 0.1)
        return response


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    example_pb2_grpc.add_ExampleServiceServicer_to_server(
        ExampleServicer(), server)

    print('Starting server. Listening on port 50050.')
    server.add_insecure_port('[::]:50050')
    server.start()

    try:
        while True:
            time.sleep(86400)

    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()
